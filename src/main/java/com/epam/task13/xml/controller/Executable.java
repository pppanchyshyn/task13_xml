package com.epam.task13.xml.controller;

import java.util.Collection;

public interface Executable {

  Collection<String> execute();
}
