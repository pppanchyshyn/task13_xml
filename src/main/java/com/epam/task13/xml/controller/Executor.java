package com.epam.task13.xml.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.*;

public abstract class Executor implements Executable {

  protected Logger logger = LogManager.getLogger(Executor.class);
  protected Properties properties;
  protected List<String> results;
  protected File xml;

  protected Executor() {
    properties = new Properties();
    results = new ArrayList<>();
    loadProperties();
    xml = new File(properties.getProperty("OldCardXML"));
  }

  private void loadProperties() {
    try {
      properties.load(new FileInputStream("src/main/resources/path.properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
