package com.epam.task13.xml.controller.dom;

import com.epam.task13.xml.controller.Executor;
import com.epam.task13.xml.model.OldCard;
import java.util.Collection;
import java.util.List;
import org.w3c.dom.Document;

public class DomController extends Executor {

  public DomController() {
  }

  private List<OldCard> getOldCardsList() {
    DomDocCreator docCreator = new DomDocCreator(xml);
    Document document = docCreator.getDocument();
    DomDocReader docReader = new DomDocReader();
    return docReader.getOldCards(document);
  }

  @Override
  public Collection<String> execute() {
    results.add("DOM Parser");
    for (OldCard oldCard : getOldCardsList()
        ) {
      results.add(oldCard.toString());
    }
    return results;
  }
}