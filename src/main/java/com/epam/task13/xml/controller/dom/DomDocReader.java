package com.epam.task13.xml.controller.dom;

import com.epam.task13.xml.model.Author;
import com.epam.task13.xml.model.OldCard;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomDocReader {

  DomDocReader() {
  }

  public List<OldCard> getOldCards(Document document) {
    document.getDocumentElement().normalize();
    List<OldCard> oldCards = new ArrayList<>();
    NodeList nodeList = document.getElementsByTagName("oldCard");
    for (int i = 0; i < nodeList.getLength(); i++) {
      OldCard oldCard = new OldCard();
      List<Author> authors;
      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        oldCard.setOldCardNumber(Integer.parseInt(element.getAttribute("oldCardNumber")));
        oldCard.setTheme(element.getElementsByTagName("theme").item(0).getTextContent());
        oldCard.setType(element.getElementsByTagName("type").item(0).getTextContent());
        oldCard.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
        oldCard.setYear(
            Integer.parseInt(element.getElementsByTagName("year").item(0).getTextContent()));
        oldCard.setValuable(element.getElementsByTagName("valuable").item(0).getTextContent());
        authors = getAuthors(element.getElementsByTagName("authors"));
        oldCard.setAuthors(authors);
        oldCards.add(oldCard);
      }
    }
    return oldCards;
  }

  private List<Author> getAuthors(NodeList nodes) {
    List<Author> authors = new ArrayList<>();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      NodeList nodeList = element.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element el = (Element) node;
          authors.add(new Author(el.getTextContent()));
        }
      }
    }
    return authors;
  }
}