package com.epam.task13.xml.controller.sax;

import com.epam.task13.xml.controller.Executor;
import com.epam.task13.xml.model.OldCard;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SaxController extends Executor {

  private static SAXParserFactory saxParserFactory;

  public SaxController() {
    saxParserFactory = SAXParserFactory.newInstance();
  }

  private Collection<OldCard> getOldCardsList() {
    Collection<OldCard> oldCardList = new ArrayList<>();
    try {
      SAXParser saxParser = saxParserFactory.newSAXParser();
      SaxHandler saxHandler = new SaxHandler();
      saxParser.parse(xml, saxHandler);

      oldCardList = saxHandler.getOldCards();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }

    return oldCardList;
  }

  @Override
  public Collection<String> execute() {
    results.add("SAX Parser");
    for (OldCard oldCard : getOldCardsList()
        ) {
      results.add(oldCard.toString());
    }
    return results;
  }
}
