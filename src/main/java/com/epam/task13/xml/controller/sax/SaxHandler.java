package com.epam.task13.xml.controller.sax;

import com.epam.task13.xml.model.Author;
import com.epam.task13.xml.model.OldCard;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHandler extends DefaultHandler {

  private List<OldCard> oldCards;
  private OldCard oldCard;
  private List<Author> authors;
  private boolean bTheme;
  private boolean bType;
  private boolean bCountry;
  private boolean bYear;
  private boolean bAuthors;
  private boolean bAuthor;
  private boolean bValuable;

  SaxHandler() {
    oldCards = new ArrayList<>();
  }

  public List<OldCard> getOldCards() {
    return oldCards;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes) {
    if (qName.equalsIgnoreCase("oldCard")) {
      String oldCardNumber = attributes.getValue("oldCardNumber");
      oldCard = new OldCard();
      oldCard.setOldCardNumber(Integer.parseInt(oldCardNumber));
    } else if (qName.equalsIgnoreCase("theme")) {
      bTheme = true;
    } else if (qName.equalsIgnoreCase("type")) {
      bType = true;
    } else if (qName.equalsIgnoreCase("country")) {
      bCountry = true;
    } else if (qName.equalsIgnoreCase("year")) {
      bYear = true;
    } else if (qName.equalsIgnoreCase("authors")) {
      bAuthors = true;
    } else if (qName.equalsIgnoreCase("author")) {
      bAuthor = true;
    } else if (qName.equalsIgnoreCase("valuable")) {
      bValuable = true;
    }
  }

  public void endElement(String uri, String localName, String qName) {
    if (qName.equalsIgnoreCase("oldCard")) {
      oldCards.add(oldCard);
    }
  }

  public void characters(char ch[], int start, int length) {
    if (bTheme) {
      oldCard.setTheme(new String(ch, start, length));
      bTheme = false;
    } else if (bType) {
      oldCard.setType(new String(ch, start, length));
      bType = false;
    } else if (bCountry) {
      oldCard.setCountry(new String(ch, start, length));
      bCountry = false;
    } else if (bYear) {
      int year = Integer.parseInt(new String(ch, start, length));
      oldCard.setYear(year);
      bYear = false;
    } else if (bAuthors) {
      authors = new ArrayList<>();
      bAuthors = false;
    } else if (bAuthor) {
      Author author = new Author();
      author.setName(new String(ch, start, length));
      authors.add(author);
      bAuthor = false;
    } else if (bValuable) {
      oldCard.setValuable(new String(ch, start, length));
      oldCard.setAuthors(authors);
      bValuable = false;
    }
  }
}