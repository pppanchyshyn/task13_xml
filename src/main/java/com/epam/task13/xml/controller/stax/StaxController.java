package com.epam.task13.xml.controller.stax;

import com.epam.task13.xml.controller.Executor;
import com.epam.task13.xml.model.Author;
import com.epam.task13.xml.model.OldCard;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxController extends Executor {

  private List<OldCard> getOldCardsList() {
    List<OldCard> oldCards = new ArrayList<>();
    OldCard oldCard = null;
    List<Author> authors = null;
    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          if (name.equals("oldCard")) {
            oldCard = new OldCard();
            Attribute idAttr = startElement.getAttributeByName(new QName("oldCardNumber"));
            if (idAttr != null) {
              oldCard.setOldCardNumber(Integer.parseInt(idAttr.getValue()));
            }
          } else if (name.equals("theme")) {
            xmlEvent = xmlEventReader.nextEvent();
            assert oldCard != null;
            oldCard.setTheme(xmlEvent.asCharacters().getData());
          } else if (name.equals("type")) {
            xmlEvent = xmlEventReader.nextEvent();
            assert oldCard != null;
            oldCard.setType(xmlEvent.asCharacters().getData());
          } else if (name.equals("country")) {
            xmlEvent = xmlEventReader.nextEvent();
            assert oldCard != null;
            oldCard.setCountry(xmlEvent.asCharacters().getData());
          } else if (name.equals("year")) {
            xmlEvent = xmlEventReader.nextEvent();
            assert oldCard != null;
            oldCard.setYear(Integer.parseInt(xmlEvent.asCharacters().getData()));
          } else if (name.equals("authors")) {
            xmlEvent = xmlEventReader.nextEvent();
            authors = new ArrayList<>();
          } else if (name.equals("author")) {
            xmlEvent = xmlEventReader.nextEvent();
            assert authors != null;
            authors.add(new Author(xmlEvent.asCharacters().getData()));
          } else if (name.equals("valuable")) {
            xmlEvent = xmlEventReader.nextEvent();
            assert oldCard != null;
            oldCard.setValuable(xmlEvent.asCharacters().getData());
            oldCard.setAuthors(authors);
          }
        }
        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("oldCard")) {
            oldCards.add(oldCard);
          }
        }
      }
    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    return oldCards;
  }

  @Override
  public Collection<String> execute() {
    results.add("StAX Parser");
    for (OldCard oldCard : getOldCardsList()
        ) {
      results.add(oldCard.toString());
    }
    return results;
  }
}
