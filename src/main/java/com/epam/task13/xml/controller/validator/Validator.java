package com.epam.task13.xml.controller.validator;

import com.epam.task13.xml.controller.Executor;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

public class Validator extends Executor {

  public Validator() {
  }

  private boolean validateXMLSchema(String xsdPath, String xmlPath) {
    try {
      SchemaFactory factory =
          SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema schema = factory.newSchema(new File(xsdPath));
      javax.xml.validation.Validator validator = schema.newValidator();
      validator.validate(new StreamSource(new File(xmlPath)));
    } catch (IOException | org.xml.sax.SAXException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  @Override
  public Collection<String> execute() {
    logger.info("Validation of XML document...");
    if (validateXMLSchema(properties.getProperty("OldCardsXSD"),
        properties.getProperty("OldCardXML"))) {
      results.add("XML document is valid");
    } else {
      results.add("XML document is invalid");
    }
    return results;
  }
}

