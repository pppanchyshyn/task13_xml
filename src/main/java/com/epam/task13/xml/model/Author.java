package com.epam.task13.xml.model;

public class Author {

  private String name;

  public Author() {
  }

  public Author(String name) {
    this.name = name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Author{" +
        "name='" + name + '\'' +
        '}';
  }
}
