package com.epam.task13.xml.model;

import java.util.List;

public class OldCard {

  private int oldCardNumber;
  private String theme;
  private String type;
  private String country;
  private Integer year;
  private List<Author> authors;
  private String valuable;

  public OldCard() {
  }

  public void setOldCardNumber(int oldCardNumber) {
    this.oldCardNumber = oldCardNumber;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public void setAuthors(List<Author> authors) {
    this.authors = authors;
  }

  public void setValuable(String valuable) {
    this.valuable = valuable;
  }

  @Override
  public String toString() {
    return "OldCard{" +
        "oldCardNumber=" + oldCardNumber +
        ", theme='" + theme + '\'' +
        ", type='" + type + '\'' +
        ", country='" + country + '\'' +
        ", year=" + year +
        ", authors=" + authors +
        ", valuable='" + valuable + '\'' +
        '}';
  }
}
