package com.epam.task13.xml.view;

import com.epam.task13.xml.controller.Executable;
import com.epam.task13.xml.controller.dom.DomController;
import com.epam.task13.xml.controller.sax.SaxController;
import com.epam.task13.xml.controller.stax.StaxController;
import com.epam.task13.xml.controller.validator.Validator;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements Printable {

  private Logger logger = LogManager.getLogger(ConsoleView.class);
  private Collection<Executable> menu;

  ConsoleView() {
    menu = new ArrayList<>();
    menu.add(new Validator());
    menu.add(new DomController());
    menu.add(new SaxController());
    menu.add(new StaxController());
  }

  private void printList(Collection<String> collection) {
    collection.forEach(s -> logger.info(s));
  }

  public void print() {
    menu.forEach(s -> printList(s.execute()));
  }
}
