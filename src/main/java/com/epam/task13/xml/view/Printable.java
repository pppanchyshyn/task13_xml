package com.epam.task13.xml.view;

public interface Printable {

  void print();
}
