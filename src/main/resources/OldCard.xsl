<?xml version="1.0" encoding="utf-16"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <body>
        <h2>Old Cards</h2>
      <table border="2">
        <tr>
          <th>Theme</th>
          <th>Type</th>
          <th>Country</th>
          <th>Year</th>
          <th>Authors</th>
          <th>Valuable</th>
        </tr>
        <xsl:for-each select="oldCards/oldCard">
          <!--Sorting by year-->
          <xsl:sort select="year"/>
          <tr>
            <td>
              <xsl:value-of select="theme"/>
            </td>
            <td>
              <xsl:value-of select="type"/>
            </td>
            <td>
              <xsl:value-of select="country"/>
            </td>
            <td>
              <xsl:value-of select="year"/>
            </td>
            <td>
              <xsl:value-of select="author"/>
            </td>
            <td>
              <xsl:value-of select="valuable"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>